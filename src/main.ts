import './assets/main.css'

import {createApp} from 'vue'
import {createPinia} from 'pinia'
import piniaPluginPersist from 'pinia-plugin-persist' // 引入pinia数据持久化插件

import App from './App.vue'
import router from './router'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

// TODO: 初始化主题
import beeboatTheme from 'beeboat-ui/es/bee-theme'
import 'beeboat-ui/theme/css/index.css'
beeboatTheme.start()

// TODO: SVG
import 'virtual:svg-icons-register'
import SvgIcon from '@/components/icons/svg/Index.vue'

import 'virtual:windi.css'

// TODO
// initialize microApp
import microApp from '@micro-zoe/micro-app'
microApp.start()

const app = createApp(App)

app.use(createPinia().use(piniaPluginPersist))
app.use(router)
app.use(ElementPlus)

app.component('svg-icon', SvgIcon)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}

app.mount('#app-main')
