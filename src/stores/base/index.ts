import {defineStore} from 'pinia'

interface PlatformItemObj {
[propsName: string]: any
}
interface BaseState {
    //
    /** 当前激活的平台 */
    platformActive?: PlatformItemObj
    /** 当前打开的选项卡 */
    tabs?: any[]
    /** 控制非蜂舟平台外的microapp */
    // appControl: boolean
}
export const useBaseStore = defineStore('base-store', {

    state: (): BaseState => {
        return {
            // TODO
            // 当前激活的平台
            platformActive: undefined,
            tabs: [],
            // appControl: true
        }
    },
    actions: {
        //
        // 设置当前点击的平台为激活状态
        setPlatformActive(data : any) {
            this.platformActive = data
        },
        // 获取当前激活的平台
        getPlatformActive() {
            return this.platformActive
        },
        // 每次打开功能时，记录tab
        setAddTab(data : any) {
            // push前判断是否已经存在，存在应当先移除
            // 关闭tab的时候也吊用setAddTab
        },
        // 整体刷新时，重新获取已打开的tabs
        getOpenedTabs() {
            return this.tabs
        },
        // getAppControl() {
        //     return this.appControl
        // },
        // setAppControl(status : boolean) {
        //     this.appControl = status
        // }
    },
    persist: {
        enabled: true,
        strategies: [
            {
                key: 'base-store',
                storage: localStorage
            }
        ]
    }
})
