import {createRouter, createWebHistory} from 'vue-router'
import WelcomeView from '../views/app/welcome/index.vue'
// import WelcomeView from '../views/app/WelcomeView.vue'
// 测试注释
const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            redirect: '/welcome',
            children: []
        }, {
            path: '/welcome',
            name: 'welcome',
            component: WelcomeView
        }, {
            path: '/designer-view',
            name: 'designer',
            component: () => import ('../views/designer/IndexView.vue')
        }, {
            path: '/platform-view',
            name: 'platform',
            component: () => import ('../views/platform/Index.vue')
        }
    ]
})

export default router
