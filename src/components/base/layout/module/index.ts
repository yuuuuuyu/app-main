// import {PropType} from 'vue'

// export const layoutModuleProps = { /** 高度 */
//     height: {
//         type: String as PropType < string >,
//         default: '52px'
//     },
//     /** 每一项的宽度 */
//     itmeWidth: {
//         type: String as PropType < string >,
//         default: '140px'
//     }

// }
export interface layoutModuleProps {
    height?: string;
    itmeWidth?: string;
}
