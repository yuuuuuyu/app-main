/*
 * @Description: 消息提示组件
 * @Author: zhiyong.yu@ytever.com
 * @Date: 2022-09-14 14:38:06
 * @LastEditors: herry jiqing.yu@ytever.com
 * @LastEditTime: 2023-05-26 16:39:47
 */
import warnImg from '@/assets/images/msg/warning.png'
import deleteImg from '@/assets/images/msg/delete.png'
import {ElMessage, ElMessageBox} from 'element-plus'
import './index.scss'

export class E_Msg { // 成功弹窗
    static success(msg? : string) {
        const message = msg ?? '保存成功'
        ElMessage({
            message,
            type: 'success',
            customClass: 'ever-msg__success',
            duration: 3000,
            offset: 90,
            grouping: true
        })
    }

    // 失败
    static error(msg? : string) {
        const message = msg ?? '操作失败！'
        ElMessage({
            message,
            type: 'error',
            customClass: 'ever-msg__error',
            duration: 10000,
            offset: 90,
            showClose: true,
            grouping: true
        })
    }

    // 提示
    static warn(title? : string | undefined, msg? : string) {
        return new Promise(resolve => {
            let messageText = ''
            let messageTitle = ''
            let classStand = ''
            if (msg != undefined) {
                messageText += '<div class="ever-msg-text__content">'
                // 内容
                const msgs = (msg ?? '').split('\n')
                msgs.forEach(v => {
                    messageText += `<p>${v}</p>`
                })
                messageText += '</div>'
            } else {
                classStand = 'ever-msg-text__title--stand'
            }

            if (title != undefined && title != null) {
                messageTitle += `<div class="ever-msg-text__title ${classStand}">${title}</div>`
            }

            const message = `
                <div class="ever-msg__img ${classStand}"><img src="${warnImg}" /></div>
                <div class="ever-msg__text">
                    ${
                messageTitle + messageText
            }
                </div>
                `

            ElMessageBox.confirm(message, {
                autofocus: false,
                dangerouslyUseHTMLString: true,
                closeOnPressEscape: false,
                confirmButtonText: '确定',
                showClose: false,
                customClass: 'ever-msg__warn',
                showCancelButton: false,
                callback: () => {
                    resolve('')
                }
            })
        })
    }

    // 删除是的确认弹框
    static delete(title? : string | undefined, msg? : string) {
        return new Promise((resolve, reject) => {
            let messageText = ''
            let messageTitle = ''
            let classStand = ''

            if (msg != undefined) {
                messageText += '<div class="ever-msg-text__content">'
                // 内容
                const msgs = (msg ?? '').split('\n')
                msgs.forEach(v => {
                    messageText += `<p>${v}</p>`
                })
                messageText += '</div>'
            } else {
                classStand = 'ever-msg-text__title--stand'
            }
            if (title != undefined && title != null) {
                messageTitle += `<div class="ever-msg-text__title">${title}</div>`
            }

            if (title == undefined && msg == undefined) {
                messageTitle = `<div class="ever-msg-text__title ${classStand}">确认要删除所选数据么？</div>`
            }

            const message = `
                <div class="ever-msg__img ${classStand}"><img src="${deleteImg}" /></div>
                <div class="ever-msg__text">
                    ${
                messageTitle + messageText
            }
                </div>
                `

            ElMessageBox.confirm(message, {
                autofocus: false,
                dangerouslyUseHTMLString: true,
                closeOnPressEscape: false,
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                showClose: false,
                customClass: 'ever-msg__delete',
                showCancelButton: true,
                callback: (action : any) => {
                    if (action == 'cancel') {
                        reject('')
                    } else {
                        resolve('')
                    }
                }
            })
        })
    }

    // 确认
    static confirm(title? : string | undefined, msg? : string) {
        return new Promise((resolve, reject) => {
            let messageText = ''
            let messageTitle = ''
            let classStand = ''
            if (msg != undefined) {
                messageText += '<div class="ever-msg-text__content">'
                // 内容
                const msgs = (msg ?? '').split('\n')
                msgs.forEach(v => {
                    messageText += `<p>${v}</p>`
                })
                messageText += '</div>'
            } else {
                classStand = 'ever-msg-text__title--stand'
            }

            if (title != undefined && title != null) {
                messageTitle += `<div class="ever-msg-text__title ${classStand}">${title}</div>`
            }

            const message = `
                <div class="ever-msg__img ${classStand}"><img src="${warnImg}" /></div>
                <div class="ever-msg__text">
                    ${
                messageTitle + messageText
            }
                </div>
                `

            ElMessageBox.confirm(message, {
                autofocus: false,
                dangerouslyUseHTMLString: true,
                closeOnPressEscape: false,
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                showClose: false,
                customClass: 'ever-msg__warn',
                showCancelButton: true,
                callback: (action : any) => {
                    if (action == 'cancel') {
                        reject('')
                    } else {
                        resolve('')
                    }
                }
            })
        })
    }
}
