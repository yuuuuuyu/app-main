// 获取默认首页
export function getConfigByHomePage() {
    return import.meta.env.VITE_HOME_PAGE ?. toString() || '/'
}

// 获取默认登陆页面
export function getConfigByLoginPage() {
    return '/login'
}

// 重置密码 页面
export function getConfigByChangePassword() {
    return '/change-password'
}
// 自主修改密码 页面
export function getConfigBySelfChangePassword() {
    return '/self-change-password'
}

// 获取根 路由
export function getConfigByRootRouter() {
    return '/main'
}

// 获取 系统服务的地址
export function getConfigByHttpServeSystem() {
    return import.meta.env.VITE_HTTP_SERVE_SYSTEM ?. toString() || ''
}

/** 获取系统服务地址 蜂舟平台 */
export function getConfigByHttpServeDesigner() {
    return import.meta.env.VITE_HTTP_SERVE_DESIGNER ?. toString() || ''
}
