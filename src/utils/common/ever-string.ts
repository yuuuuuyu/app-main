/**
 * 字符串转 驼峰
 * @param str 目标字符串
 * @param spacer 间隔符号
 * @returns
 */
export function everToCamel(str: string, spacer = '-' as string) {
    const reg = new RegExp(`\\${spacer}(\\w)`, 'g')
    return str.replace(reg, (all, letter) => letter.toUpperCase())
}
