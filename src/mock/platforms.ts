export interface Platform {
    name: string;
    code: string;
    routerView: string;
    url: string;
}
export const platforms: Platform[] = [
    {
        name: '开发工作台',
        code: 'app-designer',
        routerView: '/designer-view',
        url: 'http://localhost:3033/app-designer'
    }, {
        name: '管理工作台',
        code: 'app-vue2',
        routerView: '/platform-view',
        url: 'http://localhost:7777/'
    }, {
        name: '运维工作台',
        code: 'app-vue3',
        routerView: '/platform-view',
        url: 'http://localhost:8080/'
    },
]
