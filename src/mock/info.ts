export interface FunctionItem {
    name: string;
    code: string;
}
export const info: FunctionItem[] = [
    {
        name: '主题设置',
        code: 'theme'
    }, {
        name: '修改密码',
        code: 'password'
    }, {
        name: '退出',
        code: 'logout'
    },
]
